package net.angle.omnilighting.impl;

import com.samrj.devil.math.Vec3;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author angle
 */
public class PrimaryLightingChannelsTest {

    /**
     * Test of values method, of class PrimaryLightingChannels.
     */
    @Test
    public void testValues() {
        assertEquals(16, PrimaryLightingChannels.values().length);
        assertEquals(PrimaryLightingChannels.RED, PrimaryLightingChannels.values()[0]);
        assertEquals(PrimaryLightingChannels.SDWD, PrimaryLightingChannels.values()[15]);
//        assertArrayEquals(16, PrimaryLightingChannels.values().length);
    }

    /**
     * Test of getBitShift method, of class PrimaryLightingChannels.
     */
    @Test
    public void testGetBitShift_long() {
        assertEquals(0, PrimaryLightingChannels.getBitShift(0));
        assertEquals(0, PrimaryLightingChannels.getBitShift(1));
        assertEquals(1, PrimaryLightingChannels.getBitShift(2));
        assertEquals(0, PrimaryLightingChannels.getBitShift(3));
        assertEquals(2, PrimaryLightingChannels.getBitShift(4));
        assertEquals(4, PrimaryLightingChannels.getBitShift(16));
    }

    /**
     * Test of getBitShift method, of class PrimaryLightingChannels.
     */
    @Test
    public void testGetBitShift_0args() {
        assertEquals(0, PrimaryLightingChannels.ALL.getBitShift());
        assertEquals(56, PrimaryLightingChannels.RED.getBitShift());
    }

    /**
     * Test of getSaturationValue method, of class PrimaryLightingChannels.
     */
    @Test
    public void testGetSaturationValue() {
        assertEquals(PrimaryLightingChannels.SATURATION_VALUE, PrimaryLightingChannels.ALL.getSaturationValue());
        assertEquals(0xFF, PrimaryLightingChannels.RED.getSaturationValue());
    }

    /**
     * Test of getChannel method, of class PrimaryLightingChannels.
     */
    @Test
    public void testGetChannel() {
        assertEquals(PrimaryLightingChannels.RED.bitmask, PrimaryLightingChannels.RED.getChannel(PrimaryLightingChannels.SATURATION_VALUE));
    }

    /**
     * Test of getValue method, of class PrimaryLightingChannels.
     */
    @Test
    public void testGetValue() {
        assertEquals(0xFF, PrimaryLightingChannels.RED.getValue(PrimaryLightingChannels.SATURATION_VALUE));
    }

    /**
     * Test of getAllOtherChannels method, of class PrimaryLightingChannels.
     */
    @Test
    public void testGetAllOtherChannels() {
        assertEquals(~PrimaryLightingChannels.RED.bitmask, PrimaryLightingChannels.RED.getAllOtherChannels(PrimaryLightingChannels.SATURATION_VALUE));
    }

    /**
     * Test of setChannel method, of class PrimaryLightingChannels.
     */
    @Test
    public void testSetChannel() {
        assertEquals(PrimaryLightingChannels.RED.bitmask, PrimaryLightingChannels.RED.setChannel(0, PrimaryLightingChannels.RED.bitmask));
    }

    /**
     * Test of setValue method, of class PrimaryLightingChannels.
     */
    @Test
    public void testSetValue() {
        assertEquals(PrimaryLightingChannels.RED.bitmask, PrimaryLightingChannels.RED.setValue(0, 0xFF));
    }

    /**
     * Test of saturateChannel method, of class PrimaryLightingChannels.
     */
    @Test
    public void testSaturateChannel() {
        assertEquals(PrimaryLightingChannels.RED.bitmask, PrimaryLightingChannels.RED.saturateChannel(0));
    }
}