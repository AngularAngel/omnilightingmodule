/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.angle.omnilighting.impl;

import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.angle.omnilighting.api.LightingChannel;
import net.angle.omniworld.api.space.BlockFace;
import net.angle.omniworld.impl.space.PrimaryBlockFaces;

/**
 *
 * @author angle
 */
@AllArgsConstructor
public enum PrimaryLightingChannels implements LightingChannel {
    //There are an absolute ton of things here that are not actually lighting channels.
    RED           ("Red"           , 0xFF00000000000000l),
    GREEN         ("Green"         , 0x00FF000000000000l),
    BLUE          ("Blue"          , 0x0000FF0000000000l),
    VERTICAL      ("Vertical"      , 0x000000FF00000000l),
    NORTH_DIAGONAL("North Diagonal", 0x00000000FF000000l),
    EAST_DIAGONAL ("East Diagonal" , 0x0000000000FF0000l),
    SOUTH_DIAGONAL("South Diagonal", 0x000000000000FF00l),
    WEST_DIAGONAL ("West Diagonal" , 0x00000000000000FFl),
    COLORS("Colors", RED.bitmask | GREEN.bitmask | BLUE.bitmask),
    SKY   ("Sky"   , ~COLORS.bitmask),
    SKY_DIAGONALS   ("Sky"   , SKY.bitmask - VERTICAL.bitmask),
    ALL ("All" , COLORS.bitmask | SKY.bitmask),
    NONE("None", ~ALL.bitmask),
    RG("RG", RED.bitmask | GREEN.bitmask), //First two lighting channels
    BV("BV", BLUE.bitmask | VERTICAL.bitmask), //Second two lighting channels
    NDED("NDED", NORTH_DIAGONAL.bitmask | EAST_DIAGONAL.bitmask), //Third two lighting channels
    SDWD("SDWD", SOUTH_DIAGONAL.bitmask | WEST_DIAGONAL.bitmask), //Fourth two lighting channels
    ;
    
    public static final int CHANNEL_BIT_LENGTH = 8;
    public static final long SATURATION_VALUE = ~0l;
    
    public static final LightingChannel[] SKY_CHANNELS = {VERTICAL      ,
                                                          NORTH_DIAGONAL,
                                                          EAST_DIAGONAL ,
                                                          SOUTH_DIAGONAL,
                                                          WEST_DIAGONAL };
    
    public static final LightingChannel[] SKY_DIAGONAL_CHANNELS = {NORTH_DIAGONAL,
                                                                   EAST_DIAGONAL ,
                                                                   SOUTH_DIAGONAL,
                                                                   WEST_DIAGONAL };
    
    public final @Getter String name;
    public final long bitmask;
    public final int bitshift;
    
    public static int getBitShift(long value) {
        int ret = 0;
        while((value % 2) == 0 && value != 0) {
            value /= 2;
            ret++;
        }
        return ret;
    }
    
    public int getBitShift() {
        return getBitShift(bitmask);
    }
    
    private PrimaryLightingChannels(String name, long bitmask) {
        this(name, bitmask, getBitShift(bitmask));
    }

    @Override
    public long getSaturationValue() {
        return getValue(SATURATION_VALUE);
    }

    @Override
    public int getId() {
        return ordinal();
    }

    @Override
    public long getChannel(long lighting) {
        return lighting & bitmask;
    }

    @Override
    public long getValue(long lighting) {
        return getChannel(lighting) >>> bitshift;
    }
    
    @Override
    public long getAllOtherChannels(long lighting) {
        return lighting & (~bitmask);
    }

    @Override
    public long setChannel(long lighting, long channel) {
        lighting = getAllOtherChannels(lighting);
        return lighting | channel;
    }

    @Override
    public long setValue(long lighting, long value) {
        return setChannel(lighting, value << bitshift);
    }
    
    @Override
    public long saturateChannel(long lighting) {
        return lighting | (SATURATION_VALUE & bitmask);
    }
    
    @Override
    public BlockFace getDiagonalFace() {
        switch(this) {
            case NORTH_DIAGONAL: return PrimaryBlockFaces.BACK;
            case EAST_DIAGONAL : return PrimaryBlockFaces.RIGHT;
            case SOUTH_DIAGONAL: return PrimaryBlockFaces.FRONT;
            case WEST_DIAGONAL : return PrimaryBlockFaces.LEFT;
            default:
                throw new AssertionError(this.name());
        }
    }
    
    @Override
    public Vec3i getDiagonalDir() {
        Vec3 dir = getDiagonalFace().getInwards();
        return new Vec3i((int) dir.x, (int) dir.y, (int) dir.z);
    }

    @Override
    public int getRegistryID() {
        return -1;
    }
}