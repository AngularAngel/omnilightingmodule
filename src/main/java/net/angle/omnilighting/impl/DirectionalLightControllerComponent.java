package net.angle.omnilighting.impl;

import com.samrj.devil.math.Vec3;
import java.util.Objects;
import java.util.function.Function;
import net.angle.omnientity.api.ControllerComponent;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.TimerComponent;
import net.angle.omnientity.impl.AbstractComponent;
import net.angle.omnilighting.api.DirectionalLightComponent;

/**
 *
 * @author angle
 */
public class DirectionalLightControllerComponent extends AbstractComponent implements ControllerComponent {
    private final Function<Float, Vec3> updateFunction;
    private DirectionalLightComponent directionalLight;
    private TimerComponent timer;

    public DirectionalLightControllerComponent(Entity entity, Function<Float, Vec3> updateFunction) {
        super(entity);
        this.updateFunction = updateFunction;
    }

    @Override
    public void init() {
        super.init();
        directionalLight = getEntity().getComponent(DirectionalLightComponent.class);
        timer = getEntity().getComponent(TimerComponent.class);
        if (Objects.isNull(timer))
            timer = getEntity().getEntityEnvironment().getComponents(TimerComponent.class).get(0);
    }
    
    @Override
    public void step(float f) {
        directionalLight.setDirection(updateFunction.apply(timer.getTime()));
    }
}