package net.angle.omnilighting.impl;

import com.samrj.devil.math.Vec3;
import lombok.Getter;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.impl.AbstractUpdateableComponent;
import net.angle.omnilighting.api.LightComponent;

/**
 *
 * @author angle
 */

public abstract class AbstractLight extends AbstractUpdateableComponent<Object[]> implements LightComponent {
    private @Getter Vec3 color = new Vec3();

    public AbstractLight(Entity entity) {
        super(entity);
    }

    @Override
    public void setColor(Vec3 col) {
        color.set(col);
        setHasUpdates(true);
    }
}