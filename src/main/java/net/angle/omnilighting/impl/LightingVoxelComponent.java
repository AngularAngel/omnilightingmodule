package net.angle.omnilighting.impl;

import com.samrj.devil.math.Vec3i;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omniregistry.api.Registry;
import net.angle.omniworld.api.chunks.Chunk;
import net.angle.omniworld.api.chunks.VoxelComponent;
import net.angle.omniworld.api.space.CoordinateSystem;
import net.angle.omniworld.impl.chunks.AbstractVoxelComponent;
import net.angle.omniworld.impl.chunks.datagrams.BasicSetArrayLongDatagramAction;
import net.angle.omniworld.impl.chunks.datagrams.BasicSetHomogenousLongDatagramAction;

/**
 *
 * @author angle
 */
public class LightingVoxelComponent extends AbstractVoxelComponent<Long> {
    public LightingVoxelComponent(String name, Registry<? extends VoxelComponent> registry) {
        super(name, registry, Long.class, (long) 0);
    }
    
    @Override
    public DatagramAction<Chunk> getSetActionForChunk(Chunk chunk) {
        if (chunk.isHomogenous(this)) {
            long value = chunk.get(this, 0, 0, 0);
            if (value != 0)
                return new BasicSetHomogenousLongDatagramAction(value, getId());
        } else {
            CoordinateSystem coordinateSystem = chunk.getChunkMap().getCoordinateSystem();
            int blockEdgeLengthOfChunk = coordinateSystem.getBlockEdgeLengthOfChunk();
            long[][][] values = new long[blockEdgeLengthOfChunk][blockEdgeLengthOfChunk][blockEdgeLengthOfChunk];
            chunk.forEach((x, y, z) -> {
                values[x][y][z] = chunk.get(this, x, y, z);
            });

            return new BasicSetArrayLongDatagramAction(new Vec3i(), new Vec3i(blockEdgeLengthOfChunk),
                        values, getId());
        }
        return null;
    }
}