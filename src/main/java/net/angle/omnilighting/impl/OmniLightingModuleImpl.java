package net.angle.omnilighting.impl;

import java.util.Map;
import java.util.function.Function;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.Entity;
import net.angle.omnilighting.api.DirectionalLightComponent;
import net.angle.omnilighting.api.OmniLightingModule;
import net.angle.omnilighting.api.PointLightComponent;
import net.angle.omnimodule.impl.BasicOmniModule;
import net.angle.omniregistry.api.Registry;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniserialization.api.ClassIDRegistry;
import net.angle.omniworld.api.chunks.VoxelComponent;

/**
 *
 * @author angle
 */
public class OmniLightingModuleImpl extends BasicOmniModule implements OmniLightingModule {
    public OmniLightingModuleImpl() {
        super(OmniLightingModule.class);
    }
    
    @Override
    public void populateRegistries(RegistryCollection registries) {
        Registry<VoxelComponent> chunkComponents = (Registry<VoxelComponent>) registries.getEntryByName("Chunk Components");
        
        chunkComponents.addEntry(new LightingVoxelComponent("Lighting", chunkComponents));
    }

    @Override
    public void prepComponentInterfaceRegistry(ClassIDRegistry registry) {
        registry.addClass(DirectionalLightComponent.class);
        registry.addClass(PointLightComponent.class);
    }

    @Override
    public void prepComponentClassRegistry(ClassIDRegistry registry) {
        registry.addClass(BasicDirectionalLightComponent.class);
        registry.addClass(BasicPointLightComponent.class);
    }

    @Override
    public void prepComponentConstructors(Map<Class<? extends Component>, Function<Entity, Component>> componentConstructors) {
        componentConstructors.put(BasicDirectionalLightComponent.class, BasicDirectionalLightComponent::new);
        componentConstructors.put(BasicPointLightComponent.class, BasicPointLightComponent::new);
    }
}