package net.angle.omnilighting.impl;

import com.samrj.devil.math.Vec3;
import lombok.Getter;
import net.angle.omnientity.api.Entity;
import net.angle.omnilighting.api.DirectionalLightComponent;

/**
 *
 * @author angle
 */
public class BasicDirectionalLightComponent extends AbstractLight implements DirectionalLightComponent {
    private final @Getter Vec3 direction = new Vec3(0, 1, 0);

    public BasicDirectionalLightComponent(Entity entity) {
        super(entity);
    }

    @Override
    public void setDirection(Vec3 dir) {
        direction.set(dir);
        setHasUpdates(true);
    }
}