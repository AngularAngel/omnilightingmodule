package net.angle.omnilighting.impl;

import com.samrj.devil.geo3d.Box3;
import com.samrj.devil.math.Vec3;
import lombok.Getter;
import net.angle.omnientity.api.Entity;
import net.angle.omnientity.api.PositionComponent;
import net.angle.omnilighting.api.PointLightComponent;

/**
 *
 * @author angle
 */
public class BasicPointLightComponent extends AbstractLight implements PointLightComponent {
    private @Getter float linearAttenuation = 1, quadraticAttenuation = 1;
    private @Getter PositionComponent positionComponent;
    private @Getter Box3 aabb;

    public BasicPointLightComponent(Entity entity) {
        super(entity);
    }

    @Override
    public void init() {
        super.init();
        positionComponent = getEntity().getComponent(PositionComponent.class);
        calcAabb();
    }

    @Override
    public Vec3 getPosition() {
        return positionComponent.getPosition();
    }

    @Override
    public void setLinearAttenuation(float linearAttenuation) {
        this.linearAttenuation = linearAttenuation;
        setHasUpdates(true);
    }

    @Override
    public void setQuadraticAttenuation(float quadraticAttenuation) {
        this.quadraticAttenuation = quadraticAttenuation;
        setHasUpdates(true);
    }
    
    @Override
    public void calcAabb() {
        Vec3 pos = getPosition();
        aabb = new Box3(Vec3.sub(pos, new Vec3(getBoundingRadius())), Vec3.add(pos, new Vec3(getBoundingRadius())));
    }
}