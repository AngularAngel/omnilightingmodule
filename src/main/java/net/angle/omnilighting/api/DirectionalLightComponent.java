package net.angle.omnilighting.api;

import com.google.common.collect.ImmutableList;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.math.Vec3;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.UpdateableComponent;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author angle
 */
public interface DirectionalLightComponent extends LightComponent {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(DirectionalLightComponent.class, LightComponent.class, UpdateableComponent.class, Component.class);
    }
    
    public Vec3 getDirection();
    public void setDirection(Vec3 direction);
        
    @Override
    public default void prepLightingShader(ShaderProgram shader) {
        LightComponent.super.prepLightingShader(shader);
        shader.uniformVec3("u_light_direction", getDirection());
    }

    @Override
    public default void update(Object[] data) {
        if (Objects.nonNull(data[0]))
            setDirection((Vec3) data[0]);
        if (Objects.nonNull(data[1]))
            setColor((Vec3) data[1]);
    }

    @Override
    public default Object[] getUpdate() {
        return new Object[]{getDirection(), getColor()};
    }
}