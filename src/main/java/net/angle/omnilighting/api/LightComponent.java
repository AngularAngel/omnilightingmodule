package net.angle.omnilighting.api;

import com.google.common.collect.ImmutableList;
import net.angle.omnientity.api.Component;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.math.Vec3;
import java.util.List;
import net.angle.omnientity.api.UpdateableComponent;

/**
 *
 * @author angle
 */
public interface LightComponent extends UpdateableComponent<Object[]> {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(LightComponent.class, UpdateableComponent.class, Component.class);
    }
    
    public Vec3 getColor();
    public void setColor(Vec3 color);
    
    public default void prepLightingShader(ShaderProgram shader) {
        shader.uniformVec3("u_light_color", getColor());
    }
}