package net.angle.omnilighting.api;

import java.util.Map;
import java.util.function.Function;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.Entity;
import net.angle.omnimodule.api.OmniModule;
import net.angle.omniregistry.api.RegistryCollection;
import net.angle.omniserialization.api.ClassIDRegistry;

/**
 *
 * @author angle
 */
public interface OmniLightingModule extends OmniModule {
    public void populateRegistries(RegistryCollection registries);
    public void prepComponentInterfaceRegistry(ClassIDRegistry registry);
    public void prepComponentClassRegistry(ClassIDRegistry registry);
    public void prepComponentConstructors(Map<Class<? extends Component>, Function<Entity, Component>> componentConstructors);
}