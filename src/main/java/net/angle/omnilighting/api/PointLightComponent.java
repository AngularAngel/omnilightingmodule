package net.angle.omnilighting.api;

import com.google.common.collect.ImmutableList;
import com.samrj.devil.geo3d.Box3;
import com.samrj.devil.gl.ShaderProgram;
import com.samrj.devil.math.Mat4;
import com.samrj.devil.math.Vec3;
import net.angle.omnientity.api.Component;
import net.angle.omnientity.api.UpdateableComponent;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author angle
 */
public interface PointLightComponent extends LightComponent {
    @Override
    public default List<Class<? extends Component>> getComponentInterfaceList() {
        return ImmutableList.of(PointLightComponent.class, LightComponent.class, UpdateableComponent.class, Component.class);
    }
    
    public float getLinearAttenuation();
    public float getQuadraticAttenuation();
    public void setLinearAttenuation(float linearAttenuation);
    public void setQuadraticAttenuation(float quadraticAttenuation);
    public Vec3 getPosition();
    public Box3 getAabb();
    public void calcAabb();
    
    @Override
    public default void prepLightingShader(ShaderProgram shader) {
        LightComponent.super.prepLightingShader(shader);
        shader.uniformVec3("u_light_position", getPosition());
        shader.uniform1f("u_light_linear", getLinearAttenuation());
        shader.uniform1f("u_light_quadratic", getQuadraticAttenuation());
        shader.uniform1f("u_light_radius", getBoundingRadius());
        shader.uniformMat4("u_model_matrix", getVolumeMatrix());
    }

    public default void prepOutlineShader(ShaderProgram shader) {
        shader.uniformMat4("u_model_matrix", getVolumeMatrix());
    }
        
    public default float getBoundingRadius() {
        Vec3 color = getColor();
        float maxChannel = Math.max(Math.max(color.x, color.y), color.z);

        float ret = (float) ((-getLinearAttenuation() + Math.sqrt(getLinearAttenuation() * getLinearAttenuation() -
                4 * getQuadraticAttenuation() * (getQuadraticAttenuation() - 256 * maxChannel)))
                / (2 * getQuadraticAttenuation())) / 2f;
        return ret;
    }

    public default Mat4 getVolumeMatrix() {
        return Mat4.translation(getPosition()).mult(Mat4.scaling(new Vec3(getBoundingRadius())));
    }

    @Override
    public default void update(Object[] data) {
        if (Objects.nonNull(data[0]))
            setLinearAttenuation((float) data[0]);
        if (Objects.nonNull(data[1]))
            setQuadraticAttenuation((float) data[1]);
        if (Objects.nonNull(data[2]))
            setColor((Vec3) data[2]);
    }

    @Override
    public default Object[] getUpdate() {
        return new Object[]{getLinearAttenuation(), getQuadraticAttenuation(), getColor()};
    }
}