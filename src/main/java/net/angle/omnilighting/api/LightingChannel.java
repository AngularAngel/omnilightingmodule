package net.angle.omnilighting.api;

import com.samrj.devil.math.Vec3;
import com.samrj.devil.math.Vec3i;
import net.angle.omniregistry.api.Datum;
import net.angle.omniworld.api.space.BlockFace;

/**
 *
 * @author angle
 */
public interface LightingChannel extends Datum {
    public long getChannel(long lighting);
    public long getAllOtherChannels(long lighting);
    public long getValue(long lighting);
    public long setChannel(long lighting, long channel);
    public long setValue(long lighting, long value);
    public long getSaturationValue();
    public BlockFace getDiagonalFace();
    public Vec3i getDiagonalDir();
    
    public default boolean valueIsSaturated(long lighting) {
        return getValue(lighting) == getSaturationValue();
    }
    
    public default long saturateChannel(long lighting) {
        return setValue(lighting, getSaturationValue());
    }
    
    public default long getSaturatedChannel() {
        return saturateChannel(0l);
    }
}