
module omni.lighting {
    requires static lombok;
    requires com.google.common;
    requires devil.util;
    requires java.logging;
    requires omni.module;
    requires omni.registry;
    requires omni.world;
    requires omni.network;
    requires omni.entity;
    requires omni.serialization;
    requires org.lwjgl.opengl;
    exports net.angle.omnilighting.api;
    exports net.angle.omnilighting.impl;
}