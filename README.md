[![Java](https://img.shields.io/badge/language-java-orange.svg?style=flat
)](https://www.oracle.com/java/technologies/javase-downloads.html)
[![License](https://img.shields.io/badge/license-AGPLv3-blue.svg?style=flat
)](https://gitlab.com/AngularAngel/omnicraft/-/blob/master/LICENSE)

# Omnicraft

Omnicraft is a 3d Voxel game, intended to allow for an immersive, dynamic survival sandbox experience. It's currently more of a tech demo than a game, but development is ongoing.

## About
### Developers
- @AngularAngel
### other Contributors
- <img src="https://avatars.githubusercontent.com/u/6981448" width="20" height="20">[Smashmaster](https://github.com/SmashMaster) - Maintains DevilUtil library, and contributes lots of advice.
### The game
- The development started on September 30, 2020: https://anticapitalist.party/@Angle/104956154486767889
- Omnicraft runs on lwjgl and [DevilUtil](https://github.com/SmashMaster/DevilUtil), and uses its own voxel engine.
- This game is under AGPLv3 license for more details check the [LICENSE](https://gitlab.com/AngularAngel/omnicraft/-/blob/master/LICENSE) file.
- You can receive announcements about Omnicraft on its [discord](https://discord.gg/q7Unc868bD) server.

# Run Omnicraft
## Run latest release:
1. Install [java 16 or higher](https://www.oracle.com/java/technologies/javase-downloads.html).
2. Download the latest release from https://gitlab.com/AngularAngel/omnicraft/-/releases
3. double-click the jar.
## Compile from source:
Omnicraft is tested to compile and run with Maven.
### Maven
0. Install `git` and `maven`.
1. Download https://gitlab.com/AngularAngel/devilutil and switch it to it's 'maven' branch.
2. Download https://gitlab.com/AngularAngel/omnicraft.
3. run 'mvn compile' in both projects, in order, then run Omnicraft.

## Requirements
A computer that can run Java and a decent graphics card.
