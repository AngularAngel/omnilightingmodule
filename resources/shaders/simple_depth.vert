#version 140

in vec3 in_pos;

uniform mat4 u_view_matrix;

void main()
{
    gl_Position = u_view_matrix * vec4(in_pos, 1.0);
}  
